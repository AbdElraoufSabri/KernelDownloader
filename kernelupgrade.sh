#!/bin/bash
dateSuffix=`date +"%F"`
tempDir="/tmp/new-kernel-$dateSuffix"
mkdir -p $tempDir
cd $tempDir

if ! which lynx > /dev/null; then sudo apt-get install lynx -y; fi

if [ "$(getconf LONG_BIT)" == "64" ]; then arch=amd64; else arch=i386; fi

function download() {

	downloadUrl=$(lynx -dump -listonly -dont-wrap-pre $kernelURL | grep "$1" | grep "$2" | grep "$arch" | cut -d ' ' -f 4)
	# echo "URL=$downloadUrl"
	fileName=$(basename $downloadUrl)
	# echo "Name=$fileName"
   	wget -q -c --show-progress $downloadUrl -O $fileName
}

# Kernel URL
read -p "Do you want the latest RC?" rc
case "$rc" in
   y* | Y*) kernelURL=$(lynx -dump -nonumbers http://kernel.ubuntu.com/~kernel-ppa/mainline/ | tail -1) ;;
   n* | N*) kernelURL=$(lynx -dump -nonumbers http://kernel.ubuntu.com/~kernel-ppa/mainline/ | grep -v rc | tail -1) ;;
   *) exit ;;
esac
 
read -p "Do you want the lowlatency kernel?" lowlatency
case "$lowlatency" in
   y* | Y*) lowlatency=1 ;;
   n* | n*) lowlatency=0 ;;
   *) exit ;;
esac

# Download Kernel
if [ "$lowlatency" == "0" ]; then
   echo "Downloading the latest generic kernel."
   download generic header
   download generic image
   download generic linux-modules
elif [ "$lowlatency" == "1" ]; then
   echo "Downloading the latest lowlatency kernel."
   download lowlatency header
   download lowlatency image
   download lowlatency linux-modules
fi

# Shared Kernel Header
sharedUrl=$(lynx -dump -listonly -dont-wrap-pre $kernelURL | grep all | awk '{$1=$1};1' | cut -d ' ' -f 2- | tail -1)
# echo "sharedUrl=$sharedUrl"
sharedName=$(basename $sharedUrl)
# echo "sharedName=$sharedName"
wget -c -q $sharedUrl -O $sharedName

# Install Kernel
echo "Installing Linux Kernel"
sudo dpkg -i linux*.deb

read -p "Do you want to save kernel files?" save
case "$save" in
   y* | Y*) save=1 ;;
   n* | n*) save=0 ;;
   *) exit ;;
esac

if [ "$save" == "1" ]; then
	read -p "(absolute) destination dir = " destinationDir
	let destination="$destinationDir/new-kernel-$dateSuffix"
	mkdir -p $destinationDir
	cp *.deb $destinationDir
fi

echo "Done. You may now reboot."
